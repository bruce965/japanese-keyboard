using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

namespace JPkb
{
	public class MainForm : Form
	{
		TextBox input = new TextBox();
		
		public MainForm() {
			SuspendLayout();
			
			var panel = new Panel();
			panel.BorderStyle = BorderStyle.FixedSingle;
			panel.Click += (sender, e) => input.Focus();
			panel.Cursor = input.Cursor;
			Controls.Add(panel);
			
			input.Width = 300;
			input.Font = new Font(FontFinder.GetFirstAvailable("TakaoPGothic", "MS PGothic"), 14);
			input.TextAlign = HorizontalAlignment.Center;
			input.BackColor = Color.WhiteSmoke;
			input.ForeColor = Color.Black;
			input.BorderStyle = BorderStyle.None;
			panel.Controls.Add(input);

			var help = new Label();
			help.Width = input.Width;
			help.Font = new Font(input.Font.FontFamily, 10);
			help.Text = "ESCAPE: abort   ENTER/CTRL+C: copy\nF1: dictionary   F2: translate";
			using(var g = help.CreateGraphics())
				help.Height = (int)g.MeasureString(help.Text, help.Font).Height + 2;
			panel.Controls.Add(help);
			
			//input.LostFocus += (sender, e) => hide();
			input.TextChanged += (sender, e) => {
				var cursorEnd = input.SelectionStart + input.SelectionLength;
				var pre = input.Text.Substring(0, cursorEnd);
				var post = input.Text.Substring(cursorEnd);
				
				var converted = Deromaji.Get(pre, false);
				if(converted == pre)
					return;
				
				input.Text = converted + post;
				input.SelectionStart = converted.Length;
				input.SelectionLength = 0;
			};
			input.KeyDown += (sender, e) => {
				if (e.KeyCode == Keys.Escape) {
					hide();
				} else if (e.KeyCode == Keys.Enter || (e.KeyCode == Keys.C && e.Control)) {
					var converted = Deromaji.Get(input.Text, true);
					MonoFixes.Clipboard.SetText(converted);
					hide();
				} else if (e.KeyCode == Keys.F1 || e.KeyCode == Keys.F2) {
					var converted = Deromaji.Get(input.Text, true);
					
					string url;
					switch (e.KeyCode) {
					case Keys.F1:
						url = "http://jisho.org/search/" + Uri.EscapeDataString(converted);
						break;
					case Keys.F2:
						url = "https://translate.google.com/#ja/en/" + Uri.EscapeDataString(converted);
						break;
					default:
						throw new Exception();
					}
					
					try {
						Process.Start(url);
					} catch(Win32Exception) {
						MonoFixes.Clipboard.SetText(url);
						MessageBox.Show(url, "Copied to clipboard", MessageBoxButtons.OK, MessageBoxIcon.None);
					}
					hide();
				}
			};
			
			ShowInTaskbar = false;
			TopMost = true;
			StartPosition = FormStartPosition.CenterScreen;
			FormBorderStyle = FormBorderStyle.None;
			Size = input.Size + new Size(50, 50);
			BackColor = input.BackColor;
			
			input.Location = new Point((Size - input.Size).Width / 2, (Size - input.Size).Height / 2);
			help.Location = new Point(input.Location.X, input.Location.Y + input.Height + 5);
			Size += new Size(0, help.Height);
			
			panel.Size = Size;
			
			ResumeLayout();
			
			input.Focus();
		}
		
		void hide() {
			Hide();
			// TODO: show window on global combination and hide when here
			Close();
		}
	}
}
