using System;
using System.Text.RegularExpressions;

namespace JPkb
{
	public static class Deromaji
	{
		static readonly Regex hiragana = new Regex(@"((?:|[kstnhmyrwcgzdpbv]{1,2})[yh]?[aiueo]|shi|chi|tsu|fu|ji|dzu|vu|(?:n|m)(?![aiueo]|$))", RegexOptions.Compiled);
		static readonly Regex katakana = new Regex(@"((?:|[KSTNHMYRWLCFGZDPBV]{1,2})[YH]?[AIUEO]|SHI|CHI|TSU|FU|JI|DZU|(?:N|M)(?![AIUEO]|$))", RegexOptions.Compiled);
		static readonly Regex chouonpu1 = new Regex(@"(.{1}[AIUEO])", RegexOptions.Compiled);
		static readonly Regex chouonpu2 = new Regex(@"(.{2}[AIUEO]|.MM)", RegexOptions.Compiled);
		static readonly Regex quotationSingle = new Regex(@"(?<=「[^」]*)'");
		static readonly Regex quotationDouble = new Regex(@"(?<=『[^』]*)""");
		static readonly Regex punctuation = new Regex(@".", RegexOptions.Compiled);
		
		public static string Get(string str, bool aggressive) {
			// 'aggressive' forces 'n' to become 'ん'
			if (aggressive)
				str = str + " ";
			
			str = hiragana.Replace(str, match => {
				var piece = match.Groups[1].Value.ToLowerInvariant();
				
				var sokuon = false;
				if(piece.Length > 2 && piece[0] == piece[1]) {
					sokuon = true;
					piece = piece.Substring(1);
				}
				
				switch(piece) {
				case "a": piece = "あ"; break;
				case "i": piece = "い"; break;
				case "u": piece = "う"; break;
				case "e": piece = "え"; break;
				case "o": piece = "お"; break;
					
				case "ka": piece = "か"; break;
				case "ki": piece = "き"; break;
				case "ku": piece = "く"; break;
				case "ke": piece = "け"; break;
				case "ko": piece = "こ"; break;
				case "ga": piece = "が"; break;
				case "gi": piece = "ぎ"; break;
				case "gu": piece = "ぐ"; break;
				case "ge": piece = "げ"; break;
				case "go": piece = "ご"; break;
				
				case "sa": piece = "さ"; break;
				case "shi": piece = "し"; break;
				case "su": piece = "す"; break;
				case "se": piece = "せ"; break;
				case "so": piece = "そ"; break;
				case "za": piece = "ざ"; break;
				case "ji": piece = "じ"; break;
				case "zu": piece = "ず"; break;
				case "ze": piece = "ぜ"; break;
				case "zo": piece = "ぞ"; break;
				
				case "ta": piece = "た"; break;
				case "chi": piece = "ち"; break;
				case "tsu": piece = "つ"; break;
				case "te": piece = "て"; break;
				case "to": piece = "と"; break;
				case "da": piece = "だ"; break;
				case "di": piece = "ぢ"; break;  // this is also "ji"
				case "dzu": piece = "づ"; break;
				case "de": piece = "で"; break;
				case "do": piece = "ど"; break;
				
				case "na": piece = "な"; break;
				case "ni": piece = "に"; break;
				case "nu": piece = "ぬ"; break;
				case "ne": piece = "ね"; break;
				case "no": piece = "の"; break;
				
				case "ha": piece = "は"; break;
				case "hi": piece = "ひ"; break;
				case "fu": piece = "ふ"; break;
				case "he": piece = "へ"; break;
				case "ho": piece = "ほ"; break;
				case "pa": piece = "ぱ"; break;
				case "pi": piece = "ぴ"; break;
				case "pu": piece = "ぷ"; break;
				case "pe": piece = "ぺ"; break;
				case "po": piece = "ぽ"; break;
				case "ba": case "va": piece = "ば"; break;
				case "bi": piece = "び"; break;  // this is also "vi"
				case "bu": piece = "ぶ"; break;  // this is also "vu"
				case "be": case "ve": piece = "べ"; break;
				case "bo": case "vo": piece = "ぼ"; break;
				
				case "ma": piece = "ま"; break;
				case "mi": piece = "み"; break;
				case "mu": piece = "む"; break;
				case "me": piece = "め"; break;
				case "mo": piece = "も"; break;
				
				case "ya": piece = "や"; break;
				case "yu": piece = "ゆ"; break;
				case "yo": piece = "よ"; break;
				
				case "ra": piece = "ら"; break;
				case "ri": piece = "り"; break;
				case "ru": piece = "る"; break;
				case "re": piece = "れ"; break;
				case "ro": piece = "ろ"; break;
				
				case "wa": piece = "わ"; break;
				case "wi": piece = "ゐ"; break;
				case "we": piece = "ゑ"; break;
				case "wo": piece = "を"; break;
				
				case "n": case "m": piece = "ん"; break;
				
				case "vu": piece = "ゔ"; break;
				
				case "kya": piece = "きゃ"; break;
				case "kyu": piece = "きゅ"; break;
				case "kyo": piece = "きょ"; break;
				case "gya": piece = "ぎゃ"; break;
				case "gyu": piece = "ぎゅ"; break;
				case "gyo": piece = "ぎょ"; break;
				case "sha": piece = "しゃ"; break;
				case "shu": piece = "しゅ"; break;
				case "sho": piece = "しょ"; break;
				case "cha": piece = "ちゃ"; break;
				case "chu": piece = "ちゅ"; break;
				case "cho": piece = "ちょ"; break;
				case "ja": piece = "じゃ"; break;
				case "ju": piece = "じゅ"; break;
				case "jo": piece = "じょ"; break;
				//case "ja": piece = "ぢゃ"; break;
				//case "ju": piece = "ぢゅ"; break;
				//case "jo": piece = "ぢょ"; break;
				case "nya": piece = "にゃ"; break;
				case "nyu": piece = "にゅ"; break;
				case "nyo": piece = "にょ"; break;
				case "hya": piece = "ひゃ"; break;
				case "hyu": piece = "ひゅ"; break;
				case "hyo": piece = "ひょ"; break;
				case "pya": piece = "ぴゃ"; break;
				case "pyu": piece = "ぴゅ"; break;
				case "pyo": piece = "ぴょ"; break;
				case "bya": piece = "びゃ"; break;
				case "byu": piece = "びゅ"; break;
				case "byo": piece = "びょ"; break;
				case "mya": piece = "みゃ"; break;
				case "myu": piece = "みゅ"; break;
				case "myo": piece = "みょ"; break;
				case "rya": piece = "りゃ"; break;
				case "ryu": piece = "りゅ"; break;
				case "ryo": piece = "りょ"; break;
				}
				
				// ゝ
				// ゞ
				
				if(sokuon)
					piece = "っ" + piece;
				
				return piece;
			});
			
			MatchEvaluator chouonpuFunction = match => {
				var piece = match.Groups[1].Value.ToLowerInvariant();
				
				switch(piece) {
				case "アa": piece = "アー"; break;
				case "イi": piece = "イー"; break;
				case "ウu": piece = "ウー"; break;
				case "エe": piece = "エー"; break;
				case "オo": piece = "オー"; break;
				
				case "カa": piece = "カー"; break;
				case "キi": piece = "キー"; break;
				case "クu": piece = "クー"; break;
				case "ケe": piece = "ケー"; break;
				case "コo": piece = "コー"; break;
				case "ガa": piece = "ガー"; break;
				case "ギi": piece = "ギー"; break;
				case "グu": piece = "グー"; break;
				case "ゲe": piece = "ゲー"; break;
				case "ゴo": piece = "ゴー"; break;
				
				case "サa": piece = "サー"; break;
				case "シi": piece = "シー"; break;
				case "スu": piece = "スー"; break;
				case "セe": piece = "セー"; break;
				case "ソo": piece = "ソー"; break;
				case "ザa": piece = "ザー"; break;
				case "ジi": piece = "ジー"; break;
				case "ズu": piece = "ズー"; break;
				case "ゼe": piece = "ゼー"; break;
				case "ゾo": piece = "ゾー"; break;
				
				case "タa": piece = "ター"; break;
				case "チi": piece = "チー"; break;
				case "ツu": piece = "ツー"; break;
				case "テe": piece = "テー"; break;
				case "トo": piece = "トー"; break;
				case "ダa": piece = "ダー"; break;
				case "ヂi": piece = "ヂー"; break;
				case "ヅu": piece = "ヅー"; break;
				case "デe": piece = "デー"; break;
				case "ドo": piece = "ドー"; break;
				
				case "ナa": piece = "ナー"; break;
				case "ニi": piece = "ニー"; break;
				case "ヌu": piece = "ヌー"; break;
				case "ネe": piece = "ネー"; break;
				case "ノo": piece = "ノー"; break;
				
				case "ハa": piece = "ハー"; break;
				case "ヒi": piece = "ヒー"; break;
				case "フu": piece = "フー"; break;
				case "ヘe": piece = "ヘー"; break;
				case "ホo": piece = "ホー"; break;
				case "パa": piece = "パー"; break;
				case "ピi": piece = "ピー"; break;
				case "プu": piece = "プー"; break;
				case "ペe": piece = "ペー"; break;
				case "ポo": piece = "ポー"; break;
				case "バa": piece = "バー"; break;
				case "ビi": piece = "ビー"; break;
				case "ブu": piece = "ブー"; break;
				case "ベe": piece = "ベー"; break;
				case "ボo": piece = "ボー"; break;
				
				case "ファa": piece = "ファー"; break;
				case "フィi": piece = "フィー"; break;
				case "フェe": piece = "フェー"; break;
				case "フォo": piece = "フォー"; break;
				
				case "ヴァa": piece = "ヴァー"; break;
				case "ヴィi": piece = "ヴィー"; break;
				case "ヴu": piece = "ヴー"; break;
				case "ヴェe": piece = "ヴェー"; break;
				case "ヴォo": piece = "ヴォー"; break;
				
				case "マa": piece = "マー"; break;
				case "ミi": piece = "ミー"; break;
				case "ムu": piece = "ムー"; break;
				case "メe": piece = "メー"; break;
				case "モo": piece = "モー"; break;
				
				case "ヤa": piece = "ヤー"; break;
				case "ユu": piece = "ユー"; break;
				case "ヨo": piece = "ヨー"; break;
				
				case "ラa": piece = "ラー"; break;
				case "リi": piece = "リー"; break;
				case "ルu": piece = "ルー"; break;
				case "レe": piece = "レー"; break;
				case "ロo": piece = "ロー"; break;
				
				case "ワa": piece = "ワー"; break;
				case "ヰi": piece = "ヰー"; break;
				case "ヱe": piece = "ヱー"; break;
				case "ヲo": piece = "ヲー"; break;
				
				case "ンmm": piece = "ンー"; break;
				
				case "キャa": piece = "キャー"; break;
				case "キュu": piece = "キュー"; break;
				case "キョo": piece = "キョー"; break;
				case "ギャa": piece = "ギャー"; break;
				case "ギュu": piece = "ギュー"; break;
				case "ギョo": piece = "ギョー"; break;
				case "シャa": piece = "シャー"; break;
				case "シュu": piece = "シュー"; break;
				case "ショo": piece = "ショー"; break;
				case "チャa": piece = "チャー"; break;
				case "チュu": piece = "チュー"; break;
				case "チョo": piece = "チョー"; break;
				case "ジャa": piece = "ジャー"; break;
				case "ジュu": piece = "ジュー"; break;
				case "ジョo": piece = "ジョー"; break;
				case "ヂャa": piece = "ヂャー"; break;
				case "ヂュu": piece = "ヂュー"; break;
				case "ヂョo": piece = "ヂョー"; break;
				case "ニャa": piece = "ニャー"; break;
				case "ニュu": piece = "ニュー"; break;
				case "ニョo": piece = "ニョー"; break;
				case "ヒャa": piece = "ヒャー"; break;
				case "ヒュu": piece = "ヒュー"; break;
				case "ヒョo": piece = "ヒョー"; break;
				case "ピャa": piece = "ピャー"; break;
				case "ピュu": piece = "ピュー"; break;
				case "ピョo": piece = "ピョー"; break;
				case "ビャa": piece = "ビャー"; break;
				case "ビュu": piece = "ビュー"; break;
				case "ビョo": piece = "ビョー"; break;
				case "ミャa": piece = "ミャー"; break;
				case "ミュu": piece = "ミュー"; break;
				case "ミョo": piece = "ミョー"; break;
				case "リャa": piece = "リャー"; break;
				case "リュu": piece = "リュー"; break;
				case "リョo": piece = "リョー"; break;
				}
				
				return piece.ToUpperInvariant();
			};
			
			str = chouonpu1.Replace(str, chouonpuFunction);
			str = chouonpu2.Replace(str, chouonpuFunction);
			
			str = katakana.Replace(str, match => {
				var piece = match.Groups[1].Value.ToLowerInvariant();
				
				var sokuon = false;
				if(piece.Length > 2 && piece[0] == piece[1]) {
					sokuon = true;
					piece = piece.Substring(1);
				}
				
				switch(piece) {
				case "a": piece = "ア"; break;
				case "i": piece = "イ"; break;
				case "u": piece = "ウ"; break;
				case "e": piece = "エ"; break;
				case "o": piece = "オ"; break;
				
				case "ka": case "ca": piece =  "カ"; break;
				case "ki": piece =  "キ"; break;
				case "ku": case "cu": piece = "ク"; break;
				case "ke": piece = "ケ"; break;
				case "ko": case "co": piece = "コ"; break;
				case "ga": piece = "ガ"; break;
				case "gi": piece = "ギ"; break;
				case "gu": piece = "グ"; break;
				case "ge": piece = "ゲ"; break;
				case "go": piece = "ゴ"; break;
				
				case "sa": piece = "サ"; break;
				case "shi": case "ci": piece = "シ"; break;
				case "su": piece = "ス"; break;
				case "se": case "ce": piece = "セ"; break;
				case "so": piece = "ソ"; break;
				case "za": piece = "ザ"; break;
				case "ji": piece = "ジ"; break;
				case "zu": piece = "ズ"; break;
				case "ze": piece = "ゼ"; break;
				case "zo": piece = "ゾ"; break;
				
				case "ta": piece = "タ"; break;
				case "chi": case "ti": piece = "チ"; break;
				case "tsu": piece = "ツ"; break;
				case "te": piece = "テ"; break;
				case "to": piece = "ト"; break;
				case "da": piece = "ダ"; break;
				case "di": piece = "ヂ"; break;  // this is also "ji"
				case "dzu": piece = "ヅ"; break;
				case "de": piece = "デ"; break;
				case "do": piece = "ド"; break;
				
				case "na": piece = "ナ"; break;
				case "ni": piece = "ニ"; break;
				case "nu": piece = "ヌ"; break;
				case "ne": piece = "ネ"; break;
				case "no": piece = "ノ"; break;
				
				case "ha": piece = "ハ"; break;
				case "hi": piece = "ヒ"; break;
				case "fu": piece = "フ"; break;
				case "he": piece = "ヘ"; break;
				case "ho": piece = "ホ"; break;
				case "pa": piece = "パ"; break;
				case "pi": piece = "ピ"; break;
				case "pu": piece = "プ"; break;
				case "pe": piece = "ペ"; break;
				case "po": piece = "ポ"; break;
				case "ba": piece = "バ"; break;  // this is also "va"
				case "bi": piece = "ビ"; break;  // this is also "vi"
				case "bu": piece = "ブ"; break;  // this is also "vu"
				case "be": piece = "ベ"; break;  // this is also "ve"
				case "bo": piece = "ボ"; break;  // this is also "vo"
				
				case "fa": piece = "ファ"; break;
				case "fi": piece = "フィ"; break;
				case "fe": piece = "フェ"; break;
				case "fo": piece = "フォ"; break;
				
				case "va": piece = "ヴァ"; break;
				case "vi": piece = "ヴィ"; break;
				case "vu": piece = "ヴ"; break;
				case "ve": piece = "ヴェ"; break;
				case "vo": piece = "ヴォ"; break;
				
				case "ma": piece = "マ"; break;
				case "mi": piece = "ミ"; break;
				case "mu": piece = "ム"; break;
				case "me": piece = "メ"; break;
				case "mo": piece = "モ"; break;
				
				case "ya": piece = "ヤ"; break;
				case "yu": piece = "ユ"; break;
				case "yo": piece = "ヨ"; break;
				
				case "ra": case "la": piece = "ラ"; break;
				case "ri": case "li": piece = "リ"; break;
				case "ru": case "lu": piece = "ル"; break;
				case "re": case "le": piece = "レ"; break;
				case "ro": case "lo": piece = "ロ"; break;
				
				case "wa": piece = "ワ"; break;
				case "wi": piece = "ヰ"; break;
				case "we": piece = "ヱ"; break;
				case "wo": piece = "ヲ"; break;
				
				case "n": case "m": piece = "ン"; break;
				
				case "kya": piece = "キャ"; break;
				case "kyu": piece = "キュ"; break;
				case "kyo": piece = "キョ"; break;
				case "gya": piece = "ギャ"; break;
				case "gyu": piece = "ギュ"; break;
				case "gyo": piece = "ギョ"; break;
				case "sha": piece = "シャ"; break;
				case "shu": piece = "シュ"; break;
				case "sho": piece = "ショ"; break;
				case "cha": piece = "チャ"; break;
				case "chu": piece = "チュ"; break;
				case "cho": piece = "チョ"; break;
				case "ja": piece = "ジャ"; break;
				case "ju": piece = "ジュ"; break;
				case "jo": piece = "ジョ"; break;
				//case "ja": piece = "ヂャ"; break;
				//case "ju": piece = "ヂュ"; break;
				//case "jo": piece = "ヂョ"; break;
				case "nya": piece = "ニャ"; break;
				case "nyu": piece = "ニュ"; break;
				case "nyo": piece = "ニョ"; break;
				case "hya": piece = "ヒャ"; break;
				case "hyu": piece = "ヒュ"; break;
				case "hyo": piece = "ヒョ"; break;
				case "pya": piece = "ピャ"; break;
				case "pyu": piece = "ピュ"; break;
				case "pyo": piece = "ピョ"; break;
				case "bya": piece = "ビャ"; break;
				case "byu": piece = "ビュ"; break;
				case "byo": piece = "ビョ"; break;
				case "mya": piece = "ミャ"; break;
				case "myu": piece = "ミュ"; break;
				case "myo": piece = "ミョ"; break;
				case "rya": piece = "リャ"; break;
				case "ryu": piece = "リュ"; break;
				case "ryo": piece = "リョ"; break;
				}
				
				// ヽ
				// ヾ
				
				if(sokuon)
					piece = "ッ" + piece;
				
				return piece.ToUpperInvariant();
			});
			
			str = quotationSingle.Replace(str, "」");
			str = quotationDouble.Replace(str, "』");
			
			if (aggressive)
				str = str.Substring(0, str.Length - 1);
			
			return punctuation.Replace(str, match => {
				var piece = match.Groups[0].Value;
				
				switch(piece) {
				case "{": return "｛";
				case "}": return "｝";
				case "(": return "（";
				case ")": return "）";
				case "[": return "［";
				case "]": return "］";
				//case "": return "【";
				//case "": return "】";
				case "'": return "「";
				//case "'": return "」";
				case "\"": return "『";
				//case "\"": return "』";
				
				case " ": return "・";
				case ",": return "、";
				case ".": return "。";
				case "~": return "〜";
				case ":": return "：";
				case "!": return "！";
				case "?": return "？";
				
				case "&": return "＆";
				case "#": return "＃";
				
				case "0": return "０";
				case "1": return "１";
				case "2": return "２";
				case "3": return "３";
				case "4": return "４";
				case "5": return "５";
				case "6": return "６";
				case "7": return "７";
				case "8": return "８";
				case "9": return "９";
				}
				
				return piece;
			});
		}
	}
}
