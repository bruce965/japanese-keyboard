# Japanese Keyboard #

This software provides a simple text area, you can type with your keyboard and the text will be converted in Hiragana.
If you type in UPPERCASE the text will be converted in Katakana.

Run the program to open the text area, type your Romanji text, press ESC to exit, or ENTER to copy the text and exit.

Thanks for using my software, please write to fabiogiopla@gmail.com if you want to contact me.

