﻿using System;
using System.Drawing;
using System.Linq;

namespace JPkb
{
	public static class FontFinder
	{
		public static string GetFirstAvailable(params string[] fontNames) {
			foreach (var fontName in fontNames)
				using (var font = new Font(fontName, 12))
					if (fontName == font.Name)
						return fontName;
			
			return fontNames.Last();
		}
	}
}
