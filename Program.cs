using System;
using System.Windows.Forms;

namespace JPkb
{
	static class MainClass
	{
		[STAThread]
		static void Main(string[] args) {
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new MainForm());
		}
	}
}
