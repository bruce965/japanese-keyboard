using System;
using System.Reflection;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using MonoFixes.Internal;

namespace MonoFixes
{
	public static class Clipboard
	{
		public static void SetText(string str) {
			if (tryGtkClipboardSet(str))
				return;

			if (string.IsNullOrEmpty(str))
				System.Windows.Forms.Clipboard.Clear();  // Null pointer on Windows if SetText(String.Empty)
			else
				System.Windows.Forms.Clipboard.SetText(str);
		}

		public static void Clear() {
			SetText(String.Empty);
		}

		#region GTK Clipboard

		static bool gtkClipboardTested = false;
		static object gtkClipboard;
		static PropertyInfo clipboardText;
		static MethodInfo clipboardStore;

		static bool tryGtkClipboardSet(string str) {
			if (!gtkClipboardTested) {
				gtkClipboardTested = true;
				gtkClipboard = null;

				try {
					var gtkAssembly = Reflection.GtkSharp;
					var gdkAssembly = Reflection.GdkSharp;
					var atom = gdkAssembly.GetType("Gdk.Atom");
					var clipboard = gtkAssembly.GetType("Gtk.Clipboard");
					var clipboardGet = clipboard.GetMethod("Get", new [] { atom });
					var atomIntern = atom.GetMethod("Intern", new [] { typeof(string), typeof(bool) });
					clipboardText = clipboard.GetProperty("Text", typeof(string));
					clipboardStore = clipboard.GetMethod("Store", new Type[0]);

					if (clipboardText != null && clipboardStore != null) {
						var clipboardAtom = atomIntern.Invoke(null, new object[] { "CLIPBOARD", false });

						Application.EnableVisualStyles();  // Required
						gtkClipboard = clipboardGet.Invoke(null, new [] { clipboardAtom });
					}
				} catch {
					gtkClipboard = null;
				}
			}

			if (gtkClipboard != null) {
				try {
					clipboardText.SetValue(gtkClipboard, str, null);
					clipboardStore.Invoke(gtkClipboard, null);
					return true;
				} catch {
					return false;
				}
			}

			return false;
		}

		#endregion
	}
}
