using System;
using System.Reflection;
using System.Linq;

namespace MonoFixes.Internal
{
	static class Reflection
	{
		const string GTK_SHARP_ASSEMBLY = "gtk-sharp, Version=2.12, Culture=neutral, PublicKeyToken=35e10195dab3c99f";
		const string GDK_SHARP_ASSEMBLY = "gdk-sharp, Version=2.12, Culture=neutral, PublicKeyToken=35e10195dab3c99f";

		static bool gtkSharpAssemblyLoaded = false;
		static Assembly gtkSharpAssembly;
		public static Assembly GtkSharp {
			get { return gtkSharpAssemblyLoaded ? gtkSharpAssembly : gtkSharpAssembly = Assembly.Load(GTK_SHARP_ASSEMBLY); }
		}

		static bool gdkSharpAssemblyLoaded = false;
		static Assembly gdkSharpAssembly;
		public static Assembly GdkSharp {
			get { return gdkSharpAssemblyLoaded ? gdkSharpAssembly : gdkSharpAssembly = Assembly.Load(GDK_SHARP_ASSEMBLY); }
		}
	}
}

